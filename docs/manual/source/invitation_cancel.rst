来客予定をキャンセルする
===================================================================

画面
------------------------------------------------------------------

.. image:: _static/img/invitation_cancel.png


説明
------------------------------------------------------------------

- 来社予定のキャンセルを行います


操作
------------------------------------------------------------------

- キャンセルボタンクリック

  - 該当来客予約のキャンセルを行います

  - キャンセルメールを来社予定だったお客様に送信します

  - 完了すると、来客予約一覧に遷移します


遷移ページ
------------------------------------------------------------------

- :doc:`invitation_list`

- :doc:`invitation_detail`


