.. メカマーちゃん:エントランス受付システム documentation master file, created by
   sphinx-quickstart on Wed Jan 29 12:38:01 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

メカマーちゃん:エントランス受付システム
===================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   login
   top
   invitation_list
   invitation_regist
   invitation_detail
   invitation_cancel


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
