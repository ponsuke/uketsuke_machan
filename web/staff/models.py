#-*- coding: utf-8 -*-

import datetime

from django.contrib.auth.models import User
from django.db import models
from django.utils import dateformat, timezone


class Staff(models.Model):
    """
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    tell = models.CharField(max_length=100, default=0)

    def __str__(self):
        """
        """
        return self.user.username
