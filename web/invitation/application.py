#-*- coding: utf-8 -*-

import json
import os

from django.conf import settings
from django.core.mail import get_connection, send_mail, EmailMessage, EmailMultiAlternatives
from django.http.response import FileResponse, JsonResponse, HttpResponse
from django.template.loader import render_to_string
from django.shortcuts import render

from twilio.rest import Client as TwilioClient

from staff.models import Staff

from app.context_processors import baseurl

from .forms import InvitationForm, InvitationClientForm
from .models import Invitation, Client
from .utils import make_invitation_codes, make_qrcode_img


class InvitationRegistApp:
    """
    """
    def __init__(self, request):
        """
        """
        self.request = request
        

    def get_default_data_set(self):
        """
        """
        return {
            'result': 'ok',
            'error_messages': {},
            'invitation_form': InvitationForm(),
            'client_forms': [
                InvitationClientForm(),
                InvitationClientForm(),
                InvitationClientForm(),
            ],
        }

        
    def exec(self):
        """
        """
        result = {'result': 'ok'}

        data = {
            'title': self.request.POST.get('title', ''),
            'body': self.request.POST.get('body', ''),
            'schedule_datetime_date': self.request.POST.get('schedule_datetime_date', ''),
            'schedule_datetime_hour': self.request.POST.get('schedule_datetime_hour', ''),
            'schedule_datetime_minute': self.request.POST.get('schedule_datetime_minute', ''),
        }
        
        # 招待データの入力チェック
        if self.request.POST.get('schedule_datetime_date', '') or \
           self.request.POST.get('schedule_datetime_hour', '') or \
           self.request.POST.get('schedule_datetime_minute', ''):
            data['schedule_datetime'] = '{0} {1}:{2}:00'.format(
                self.request.POST.get('schedule_datetime_date', ''),
                self.request.POST.get('schedule_datetime_hour', ''),
                self.request.POST.get('schedule_datetime_minute', ''))


        result['invitation_form'] = invitation_form = InvitationForm(data)

        if not invitation_form.is_valid():
            result['result'] = 'error'

            result.setdefault('error_messages', {})

            errors = invitation_form.errors.as_data()
            
            result['error_messages']['title'] = errors['title'][0].message if errors.get('title') else ''
            result['error_messages']['body'] = errors['body'][0].message if errors.get('body') else ''
            result['error_messages']['schedule_datetime'] = errors['schedule_datetime'][0].message if errors.get('schedule_datetime') else ''
            
        # 招待クライアントの入力チェック
        names = self.request.POST.getlist('name', [])
        emails = self.request.POST.getlist('email', [])

        client_forms = []
        
        for i in range(len(names)):
            data = {
                'name': names[i],
                'email': emails[i],
            }

            form = InvitationClientForm(data)

            client_forms.append(form)
            
            if not form.is_valid():
                result['result'] = 'error'

                errors = form.errors.as_data()
                
                result.setdefault('error_messages', {}) 
                result['error_messages'].setdefault('name', []) 
                result['error_messages'].setdefault('email', []) 

                result['error_messages']['name'].append(errors['name'][0].message if errors.get('name') else '')
                result['error_messages']['email'].append(errors['email'][0].message if errors.get('email') else '')
            else:
                result.setdefault('error_messages', {}) 
                result['error_messages'].setdefault('name', []) 
                result['error_messages'].setdefault('email', [])
                result['error_messages']['name'].append('')
                result['error_messages']['email'].append('')


            result['client_forms'] = client_forms
            
        # 登録処理
        if 'ok' == result['result']:

            # 招待データ登録
            invitation = Invitation(
                user=self.request.user,
                title=invitation_form.cleaned_data.get('title', ''),
                body=invitation_form.cleaned_data.get('body', ''),
                schedule_datetime=invitation_form.cleaned_data.get('schedule_datetime', '')
            )
            
            invitation.save()

            # 招待クライアント登録
            codes = make_invitation_codes(nums=len(client_forms))

            clients = []
            
            for i, code in enumerate(codes):

                client_form = client_forms[i]

                client = Client(
                    invitation=invitation,
                    invitation_code=code,
                    name=client_form.cleaned_data.get('name', ''),
                    email=client_form.cleaned_data.get('email', '')
                )

                client.save()

                clients.append(client)
                
                qrcode_img = make_qrcode_img(code)

            # HTMLメール飛ばす
            self.send_mail(invitation, clients)

        return result


    def send_mail(self, invitation, clients):
        """
        """
        with get_connection() as connection:

            for client in clients:

                qrcode = self.request.META['HTTP_ORIGIN'] + '/invitation/qr/' + client.invitation_code

                body = render_to_string('invitation/mail.html', {
                    'client_name': client.name,
                    'title': invitation.title,
                    'schedule_datetime': invitation.schedule_datetime,
                    'qrcode': qrcode,
                })

                message = EmailMessage(invitation.title, body, settings.EMAIL_HOST_USER, [client.email], connection=connection)
                message.content_subtype = 'html'
                message.send()

        return True
