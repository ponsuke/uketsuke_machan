#-*- coding: utf-8 -*-

import json
import os
import urllib.parse

from django.conf import settings
from django.core.mail import get_connection, send_mail, EmailMessage, EmailMultiAlternatives
from django.http.response import JsonResponse, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.template.loader import render_to_string
from django.urls import reverse
from django.views import generic

import boto3
import requests
from twilio.rest import Client as TwilioClient

from staff.models import Staff

from .application import InvitationRegistApp
from .defines import HOURS, MINUTES
from .forms import InvitationForm, InvitationClientForm
from .models import Invitation, Client
from .utils import make_invitation_codes


# Create your views here.
class IndexView(generic.ListView):

    template_name = 'invitation/index.html'
    context_object_name = 'invitation_datas'

    paginate_by = 10

    def get_context_data(self, **kwargs):
        """
        """
        context = super().get_context_data(**kwargs)

        get_query = {}
        
        for key in self.request.GET:
            if 'page' == key:
                pass
            elif 'staff' == key:
                try:
                    get_query.setdefault(key, int(self.request.GET[key]))
                except(ValueError):
                    get_query.setdefault(key, '')
            else:
                get_query.setdefault(key, self.request.GET[key])

        context['get'] = get_query
        context['query_string'] = urllib.parse.urlencode(get_query)

        context['HOURS'] = HOURS
        context['MINUTES'] = MINUTES
        
        staffs = Staff.objects.all().order_by('id')
        context['staffs'] = staffs

        return context
    
    def get_queryset(self):
        """Return the last five published questions.
        """
        invitations = Invitation.objects.all().order_by('id')

        if self.request.GET.get('title'):
            invitations = invitations.filter(title__contains=self.request.GET.get('title'))

        if self.request.GET.get('staff'):
            invitations = invitations.filter(user__staff__id=self.request.GET.get('staff'))

        if self.request.GET.get('start_schedule_datetime_date', '') and \
           self.request.GET.get('start_schedule_datetime_hour', '') and \
           self.request.GET.get('start_schedule_datetime_minute', ''):
            start_schedule_datetime = '{0} {1}:{2}:00'.format(
                self.request.GET.get('start_schedule_datetime_date', ''),
                self.request.GET.get('start_schedule_datetime_hour', ''),
                self.request.GET.get('start_schedule_datetime_minute', ''))
            invitations = invitations.filter(schedule_datetime__gte=start_schedule_datetime)

        if self.request.GET.get('end_schedule_datetime_date', '') and \
           self.request.GET.get('end_schedule_datetime_hour', '') and \
           self.request.GET.get('end_schedule_datetime_minute', ''):
            end_schedule_datetime = '{0} {1}:{2}:00'.format(
                self.request.GET.get('end_schedule_datetime_date', ''),
                self.request.GET.get('end_schedule_datetime_hour', ''),
                self.request.GET.get('end_schedule_datetime_minute', ''))
            invitations = invitations.filter(schedule_datetime__lte=end_schedule_datetime)

        return invitations


class DetailView(generic.DetailView):

    model = Invitation
    template_name = 'invitation/detail.html'


def regist(request):
    """
    """
    app = InvitationRegistApp(request)

    if 'POST' == request.method:

        data = app.exec()

        if 'ok' == data['result']:
            return HttpResponseRedirect(reverse('invitation:index'))

    else:
        data = app.get_default_data_set()

    data['HOURS'] = HOURS
    data['MINUTES'] = MINUTES
    
    return render(request, 'invitation/regist.html', data)
    

def cancel(request, pk):
    """
    """
    if 'POST' == request.method:
        invitation = get_object_or_404(Invitation, pk=pk)
        invitation.cancel_flag = 1
        invitation.save()

        with get_connection() as connection:

            for client in invitation.client_set.all():

                body = render_to_string('invitation/cancel_mail.txt', {
                    'client_name': client.name,
                    'title': invitation.title,
                    'schedule_datetime': invitation.schedule_datetime,
                })

                message = EmailMessage(invitation.title, body, settings.EMAIL_HOST_USER, [client.email], connection=connection)

                message.send()
          
    return HttpResponseRedirect(reverse('invitation:index'))


def show_invitation_code(request, invitation_code):
    """
    """
    img_path = os.path.join(settings.QRCODE_PATH, invitation_code + '.png')

    f = open(img_path, 'rb')

    return HttpResponse(f.read(), content_type='image/png')
    

def check_invitation_code(request, invitation_code):
    """
    """
    try:
        client = Client.objects.filter(invitation_code=invitation_code)[0]

        invitation = client.invitation
    
        #tell = client.invitation.user.staff.tell.replace('-', '')

        body = """
{} の件で、{} 様がお越しになりました。
2F エントランスまでお迎えに上がってください。
""".format(invitation.title, client.name)

        if 0 == client.is_checkin:

            # amazon sns
            #sns = boto3.client("sns")
            #message_attributes = {
            #    'AWS.SNS.SMS.SenderID': {
            #        'DataType': 'String',
            #        'StringValue': 'northshore' # 通知者表示に使用される送信者ID
            #    }
            #}
            #sns.publish(PhoneNumber="+81" + tell, Message=body, MessageAttributes=message_attributes)

            # slack
            params = {
                'token'  : settings.SLACK_TOKEN,
                'channel': settings.SLACK_CHANNEL,
                'text'   : "<!channel> " + body,
            }

            session = requests.Session()

            session.post(settings.SLACK_URL, data=params)

            client.is_checkin = 1

            client.save()

        return JsonResponse({'result': 'ok'})

    except IndexError:
        return JsonResponse({'result': 'error', 'message': 'データが存在しません'})
