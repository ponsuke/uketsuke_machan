#-*- coding: utf-8 -*-

from datetime import datetime, timedelta
import json
import os

from django.conf import settings
from django.contrib.auth.models import User

from django.test import Client as TestClient
from django.test import RequestFactory, TestCase

from staff.models import Staff

from .application import InvitationRegistApp
from .forms import InvitationForm, InvitationClientForm
from .models import Invitation, Client
from .utils import make_qrcode_img, make_invitation_codes


# Create your tests here.
class InvitationSearchTestCase(TestCase):
    """
    """
    def setUp(self):
        """
        """
        self.client = TestClient()

        self.user = User.objects.create_user('Masaru', 'upw.ponsuke@gmail.com', 'password')

        self.client.login(username='Masaru', password='password')

        staff = Staff(user=self.user, tell='090-6226-5008')
        staff.save()

        self.past_schedule_datetime = datetime.today() + timedelta(days=-1)
        
        self.current_schedule_datetime = datetime.today()

        self.future_schedule_datetime = datetime.today() + timedelta(days=1)

        self.invitation = Invitation(
            user=self.user,
            title='Past Schedule Title',
            body='Past Schedule Body',
            schedule_datetime=self.past_schedule_datetime.strftime('%Y-%m-%d %H:%M:%S'))
        self.invitation.save()

        self.invitation = Invitation(
            user=self.user,
            title='Current Schedule Title',
            body='Current Schedule Body',
            schedule_datetime=self.current_schedule_datetime.strftime('%Y-%m-%d %H:%M:%S'))
        self.invitation.save()

        self.invitation = Invitation(
            user=self.user,
            title='Future Schedule Title',
            body='Future Schedule Body',
            schedule_datetime=self.future_schedule_datetime.strftime('%Y-%m-%d %H:%M:%S'))
        self.invitation.save()

    def test_search(self):
        """
        """
        query = {
            "start_schedule_datetime_date": "",
            "start_schedule_datetime_hour": "",
            "start_schedule_datetime_minute": "",
            "end_schedule_datetime_date": "",
            "end_schedule_datetime_hour": "",
            "end_schedule_datetime_minute": "",
        }
        
        response = self.client.get("/invitation/", query)
        self.assertEqual(3, len(response.context["invitation_datas"]))
        self.assertEqual(200, response.status_code)

    def test_today_search(self):
        """
        """
        query = {
            "start_schedule_datetime_date": self.current_schedule_datetime.strftime('%Y-%m-%d'),
            "start_schedule_datetime_hour": "0",
            "start_schedule_datetime_minute": "0",
            "end_schedule_datetime_date": self.current_schedule_datetime.strftime('%Y-%m-%d'),
            "end_schedule_datetime_hour": "23",
            "end_schedule_datetime_minute": "59",
        }
        
        response = self.client.get("/invitation/", query)
        self.assertEqual(1, len(response.context["invitation_datas"]))
        self.assertEqual(200, response.status_code)
        
class InvitationTestCase(TestCase):
    """
    """
    def setUp(self):
        """
        """
        self.client = TestClient()

        self.user = User.objects.create_user('Masaru', 'upw.ponsuke@gmail.com', 'password')

        self.client.login(username='Masaru', password='password')
        
        staff = Staff(user=self.user, tell='090-6226-5008')
        staff.save()

        self.invitation = Invitation(
            user=self.user,
            title='Test Title',
            body='Test Body',
            schedule_datetime=datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
        self.invitation.save()

        self.codes = make_invitation_codes(nums=1)

        for code in self.codes:
            make_qrcode_img(code)

        client = Client(
            invitation=self.invitation,
            invitation_code=self.codes[0],
            name='Fukazawa Masaru',
            email='upw.ponsuke@gmail.com'
        )
        client.save()


    def test_make_make_qrcode_img(self):
        """ QRコード生成テスト
        """
        path = make_qrcode_img(self.codes[0])

        self.assertEqual(path, os.path.join(settings.QRCODE_PATH, self.codes[0] + '.png'))

        
    def test_make_invitation_code(self):
        """ 招待コード生成テスト
        """
        codes = make_invitation_codes(nums=5)

        self.assertEqual(5, len(codes))
        self.assertEqual(100, len(codes[0]))
        self.assertEqual(100, len(codes[1]))
        self.assertEqual(100, len(codes[2]))
        self.assertEqual(100, len(codes[3]))
        self.assertEqual(100, len(codes[4]))


    def test_ok_Invitation_form(self):
        """ 招待フォーム OK
        """
        data = {
            'title': 'a' * 100,
            'body': 'b' * 500,
            'schedule_datetime': (datetime.today() + timedelta(minutes=10)).strftime('%Y-%m-%d %H:%M:%S')
        }
        
        form = InvitationForm(data)

        self.assertTrue(form.is_valid())


    def test_error_Invitation_form(self):
        """ 招待フォーム Error
        """
        # 未入力
        data = {}
        
        form = InvitationForm(data)

        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors.as_data()['title'][0].message, '必須です')
        self.assertEqual(form.errors.as_data()['body'][0].message, '必須です')
        self.assertEqual(form.errors.as_data()['schedule_datetime'][0].message, '必須です')


        # 未入力
        data = {
            'title': '',
            'body': '',
            'schedule_datetime': '',
        }
        
        form = InvitationForm(data)

        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors.as_data()['title'][0].message, '必須です')
        self.assertEqual(form.errors.as_data()['body'][0].message, '必須です')
        self.assertEqual(form.errors.as_data()['schedule_datetime'][0].message, '必須です')


        # 範囲外
        data = {
            'title': 'a' * 101,
            'body': 'a' * 501,
            'schedule_datetime': '2019-02-30 00:00:00',
        }
        
        form = InvitationForm(data)

        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors.as_data()['title'][0].message, '100文字以内です')
        self.assertEqual(form.errors.as_data()['body'][0].message, '500文字以内です')
        self.assertEqual(form.errors.as_data()['schedule_datetime'][0].message, '日付/時間を正しく入力してください')        
        


    def test_ok_Invitation_client_form(self):
        """ 招待クライアント OK
        """
        data = {
            'name': 'a' * 100,
            'email': 'upw.ponsuke@gmail.com',
        }

        form = InvitationClientForm(data)

        self.assertTrue(form.is_valid())


    def test_error_Invitation_client_form(self):
        """ 招待クライアント Error
        """
        data = {
            'name': '',
            'email': '',
        }

        form = InvitationClientForm(data)

        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors.as_data()['name'][0].message, '必須です')
        self.assertEqual(form.errors.as_data()['email'][0].message, '必須です')

        
        data = {
            'name': 'a' * 101,
            'email': 'upw.ponsukegmail.com',
        }

        form = InvitationClientForm(data)

        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors.as_data()['name'][0].message, '100文字以内です')
        self.assertEqual(form.errors.as_data()['email'][0].message, '有効なメールアドレスを入力してください')


    def test_ok_application_regist(self):
        """ 登録処理 OK
        """
        schedule_datetime = datetime.today() + timedelta(minutes=10)
        
        data = {
            'title': 'a' * 100,
            'body': 'b' * 500,
            'schedule_datetime_date': schedule_datetime.strftime('%Y-%m-%d'),
            'schedule_datetime_hour': schedule_datetime.strftime('%H'),
            'schedule_datetime_minute': schedule_datetime.strftime('%M'),
            'name': [
                'a' * 100,
                'b' * 100,
                'c' * 100,
            ],
            'email': [
                'upw.ponsuke+001@gmail.com',
                'upw.ponsuke+002@gmail.com',
                'upw.ponsuke+003@gmail.com',
            ],
        }

        factory = RequestFactory()
        
        request = factory.post('/invitation/regist', data)

        request.user = self.user

        app = InvitationRegistApp(request)
        result = app.exec()

        self.assertEqual('ok', result['result'])


    def test_error_application_regist(self):
        """ 登録処理 Error
        """
        schedule_datetime = datetime.today() + timedelta(minutes=10)
        
        data = {
            'title': '',
            'body': '',
            'schedule_datetime_date': '',
            'schedule_datetime_hour': '',
            'schedule_datetime_minute': '',
            'name': ['', '', '',],
            'email': ['', '', '',],
        }

        factory = RequestFactory()
        
        request = factory.post('/invitation/regist/', data)

        request.user = self.user

        app = InvitationRegistApp(request)
        result = app.exec()

        self.assertEqual('error', result['result'])
        self.assertEqual('必須です', result['error_messages']['title'])
        self.assertEqual('必須です', result['error_messages']['body'])
        self.assertEqual('必須です', result['error_messages']['name'][0])
        self.assertEqual('必須です', result['error_messages']['email'][0])

        
    def test_index(self):
        """ 一覧画面 OK
        """
        response = self.client.get('/invitation/')

        self.assertEqual(200, response.status_code)

    def test_detail(self):
        """ 詳細画面 OK
        """
        response = self.client.get('/invitation/{}'.format(self.invitation.id))

        self.assertEqual(200, response.status_code)


    def test_ok_regist(self):
        """ 登録画面 OK
        """
        schedule_datetime = datetime.today() + timedelta(minutes=10)
        
        data = {
            'title': 'a' * 100,
            'body': 'b' * 500,
            'schedule_datetime_date': schedule_datetime.strftime('%Y-%m-%d'),
            'schedule_datetime_hour': schedule_datetime.strftime('%H'),
            'schedule_datetime_minute': schedule_datetime.strftime('%M'),
            'name': [
                'a' * 100,
                'b' * 100,
                'c' * 100,
            ],
            'email': [
                'upw.ponsuke+001@gmail.com',
                'upw.ponsuke+002@gmail.com',
                'upw.ponsuke+003@gmail.com',
            ],
        }

        response = self.client.post('/invitation/regist', data)

        self.assertEqual(302, response.status_code)

    def test_error_regist(self):
        """ 登録画面 Error
        """
        schedule_datetime = datetime.today() + timedelta(minutes=10)
        
        data = {
            'title': '',
            'body': '',
            'schedule_datetime_date': '',
            'schedule_datetime_hour': '',
            'schedule_datetime_minute': '',
            'name': ['',  '', '',],
            'email': ['',  '', '',],
        }

        response = self.client.post('/invitation/regist', data)

        self.assertEqual(200, response.status_code)

        response = self.client.get('/invitation/regist', data)

        self.assertEqual(200, response.status_code)
        

    def test_show_invitation_code(self):
        """ QRコード参照
        """
        response = self.client.get('/invitation/qr/' + self.codes[0])

        self.assertEqual(200, response.status_code)
        
        
    def test_ok_check_invitation_code(self):
        """ 招待コードチェック OK
        """
        response = self.client.get('/invitation/qr/check/' + self.codes[0])
        
        self.assertEqual(200, response.status_code)
        res = json.loads(response.content)
        self.assertEqual('ok', res['result'])


    def test_Error_check_invitation_code(self):
        """ 招待コードチェック Error
        """
        response = self.client.get('/invitation/qr/check/' + ('0' * 100))
        
        self.assertEqual(200, response.status_code)

        res = json.loads(response.content)

        self.assertEqual('error', res['result'])
        self.assertEqual('データが存在しません', res['message'])
