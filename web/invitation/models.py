#-*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.db import models
from django.utils import dateformat, timezone

from staff.models import Staff

class Invitation(models.Model):
    """
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, default='')
    body  = models.CharField(max_length=500, default='')
    schedule_datetime = models.DateTimeField(auto_now=False)
    cancel_flag = models.IntegerField(default=0)
    upd_datetime = models.DateTimeField(auto_now=True)
    ins_datetime = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        """
        """
        return self.title


class Client(models.Model):
    """
    """
    invitation = models.ForeignKey(Invitation, on_delete=models.CASCADE)
    invitation_code = models.CharField(max_length=100, default='', unique=True)
    name = models.CharField(max_length=100, default='')
    email = models.EmailField()
    is_checkin = models.IntegerField(default=0)
    upd_datetime = models.DateTimeField(auto_now=True)
    ins_datetime = models.DateTimeField(auto_now_add=True)    


    def __str__(self):
        """
        """
        return self.name
