#-*- coding: utf-8 -*-

HOURS = [str(i) for i in range(0, 24)]

MINUTES = [str(i) for i in range(0, 60, 10)]
