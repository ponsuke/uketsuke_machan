#-*- coding: utf-8 -*-

from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.urls import path

from . import views


app_name = 'invitation'

urlpatterns = [
    path('', login_required(views.IndexView.as_view()), name='index'),
    path('<int:pk>', login_required(views.DetailView.as_view()), name='detail'),
    path('<int:pk>/cancel', login_required(views.cancel), name='cancel'),
    path('regist', login_required(views.regist), name='regist'),
    path('qr/<str:invitation_code>', views.show_invitation_code, name='show_invitation_code'),
    path('qr/check/<str:invitation_code>', views.check_invitation_code, name='check_invitation_code'),
]
