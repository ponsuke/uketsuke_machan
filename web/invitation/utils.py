#-*- coding: utf-8 -*-

from datetime import datetime
import random
import os

from django.conf import settings

import qrcode


def make_qrcode_img(code):
    """ QRコード作成
    """
    img = qrcode.make(code)

    img_path = os.path.join(settings.QRCODE_PATH, code + '.png')

    resize_img = img.resize((200, 200))
    resize_img.save(img_path)
    
    return img_path


def make_invitation_codes(nums=5):
    """ 招待コード生成
    """
    now = datetime.today().strftime('%Y%m%d%H%M%S')

    letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

    rand_code = ''.join(random.choices(letters, k=81))
    
    codes = []
    
    for i in range(1, nums+1):
        codes.append(now + '_' + rand_code + '_' + ('{:0>3}'.format(i)))

    return codes
