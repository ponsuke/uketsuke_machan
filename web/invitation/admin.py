#-*- coding: utf-8 -*-

from django.contrib import admin

# Register your models here.
from .models import Invitation, Client


class InvitationAdmin(admin.ModelAdmin):
    fields = ['user', 'title', 'schedule_datetime', 'cancel_flag']

admin.site.register(Invitation, InvitationAdmin)


class ClientAdmin(admin.ModelAdmin):
    fields = ['invitation', 'name', 'email', 'is_checkin']

admin.site.register(Client, ClientAdmin)
