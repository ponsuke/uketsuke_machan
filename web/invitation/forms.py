#-*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.models import User

from .models import Invitation, Client


class InvitationForm(forms.ModelForm):
    '''
    '''
    class Meta:
        model = Invitation

        fields = [
            'title',
            #'body',
            'schedule_datetime',
        ]

        error_messages = {
            'title': {
                'required': '必須です',
                'max_length': '100文字以内です',
            },
            #'body': {
            #    'required': '必須です',
            #    'max_length': '500文字以内です',
            #},
            'schedule_datetime': {
                'required': '必須です',
                'invalid': '日付/時間を正しく入力してください',
            },
        }


class InvitationClientForm(forms.ModelForm):
    '''
    '''
    class Meta:
        model = Client

        fields = [
            'name',
            'email',
        ]

        error_messages = {
            'name': {
                'required': '必須です',
                'max_length': '100文字以内です',
            },
            'email': {
                'required': '必須です',
                'invalid': '有効なメールアドレスを入力してください',
            },
        }
