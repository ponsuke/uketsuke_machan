#-*- coding: utf-8 -*-

from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.urls import path

from . import views


app_name = 'top'

urlpatterns = [
    path('', login_required(views.IndexView.as_view()), name='index'),
]
