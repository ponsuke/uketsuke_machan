#-*- coding: utf-8 -*-

import json
import os
from datetime import timedelta

from django.conf import settings
from django.http.response import JsonResponse, HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone
from django.views import generic

from invitation.models import Invitation


# Create your views here.
class IndexView(generic.ListView):

    template_name = 'top/index.html'
    context_object_name = 'invitation_datas'

    def get_context_data(self, **kwargs):
        """
        """
        context = super().get_context_data(**kwargs)
        return context
    
    def get_queryset(self):
        """Return the last five published questions.
        """

        now_datetime = timezone.now()

        start_datetime = now_datetime.strftime('%Y-%m-%d 00:00:00')
        end_datetime   = (now_datetime + timedelta(days=1)).strftime('%Y-%m-%d 00:00:00')
        
        return Invitation.objects.filter(
            schedule_datetime__gte=start_datetime,
            schedule_datetime__lt=end_datetime
        ).order_by('schedule_datetime')
