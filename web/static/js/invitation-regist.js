$(function() {

    $(".datepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        closeText: "閉じる",
        prevText: "&#x3C;前",
        nextText: "次&#x3E;",
        currentText: "今日",
        monthNames: [ "1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月" ],
        monthNamesShort: [ "1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月" ],
        dayNames: [ "日曜日","月曜日","火曜日","水曜日","木曜日","金曜日","土曜日" ],
        dayNamesShort: [ "日","月","火","水","木","金","土" ],
        dayNamesMin: [ "日","月","火","水","木","金","土" ],
        weekHeader: "週",
        yearSuffix: "年"
    });


    function init_delete_client_buttons () {
    
        $(".delete_client_buttons").each(function(){

            $(this).unbind("clicke");
	    
            $(this).click(function(){

                $(this).parent().parent().parent().parent().parent().remove();

            });
	
        });

    }

    $("#add_client_button").click(function(){

	    var html = "";
        
	    html += '<table class="table table-bordered"><tr><td scope="row" style="width: 30%"><p class="require">※名前</p></td><td><input id="inputName" type="text" name="name" value="" placeholder="" class="form-control"/></td></tr>';
	    html += '<tr><td scope="row"><p class="require">※メールアドレス</p></td><td><input id="inputMail" type="text" name="email" value="" placeholder="" class="form-control"/></td></tr>';
	    html += '<tr><td scope="row"></td><td><span class="float-xl-right"><button type="button" class="btn btn-sm btn-danger delete_client_buttons">削除</button></span></td></tr></table>';
        
	    $("#clients").append(html);
        
	    init_delete_client_buttons();
	
    });

    init_delete_client_buttons();
    
}());
