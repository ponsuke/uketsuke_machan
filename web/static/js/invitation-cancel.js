$(function() {

    $("#invitation_cancel").click(function(){

        if (confirm("この来客予約をキャンセルしてもいいですか？")) {
            $("#invitation_cancel_form").submit();
        }

        return false;
    });

}());
