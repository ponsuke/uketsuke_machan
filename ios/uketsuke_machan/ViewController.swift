//
//  ViewController.swift
//  uketsuke_machan
//
//  Created by 深澤勝 on 2018/11/09.
//  Copyright © 2018年 northshore, inc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var qrButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        self.qrButton.layer.borderColor = UIColor.white.cgColor
        self.qrButton.layer.borderWidth = 2
        
    }

    @IBAction func goToSetting(_ sender: Any) {
        let next = self.storyboard!.instantiateViewController(withIdentifier: "SettingChange")
        self.present(next, animated: false, completion: nil)
    }
    
}

