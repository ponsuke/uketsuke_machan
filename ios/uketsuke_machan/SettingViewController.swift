//
//  InitViewController.swift
//  uketsuke_machan
//
//  Created by 深澤勝 on 2019/01/28.
//  Copyright © 2019年 northshore, inc. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

//let url_pattern: String = "^(http|https)://([¥w-]+¥.)+[¥w-]+(/[¥w-./?%&=]*)?$"

/*
extension String {

    func isUrl() throws -> Bool {

        //let url_pattern = "^(http|https)://([¥w-]+¥.)+[¥w-]+(/[¥w-./?%&=]*)?$"

        guard let regex = try? NSRegularExpression(pattern: "^http(s)?://") else { return false }

        let matches = regex.matches(in: self, range: NSMakeRange(0, 10000))
        
        return matches.count > 0
    }

}
*/

class SettingViewController: UIViewController {
    
    @IBOutlet weak var url: UITextField!
    
    override func viewDidLoad() {

        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.
        /*
        let realm = try! Realm()
        
        try! realm.write {
            realm.deleteAll()
        }
        */

    }
    
    override func viewDidAppear(_ animated: Bool) {

        // 初期値を与える。
        let realm = try! Realm()
        let setting = realm.objects(SettingModel.self).first
        
        // 画面遷移
        if (setting != nil) {
            let next = self.storyboard!.instantiateViewController(withIdentifier: "Top")
            self.present(next, animated: false, completion: nil)
        }

    }
    
    @IBAction func save(_ sender: Any) {
        
        var url_txt: String = ""
        
        if "" != self.url.text! {
            url_txt = self.url.text!
        }
        
        // 入力チェック
        if url_txt != "" {

            let realm = try! Realm()
            
            try! realm.write {
                realm.deleteAll()
            }
            
            let setting = SettingModel()
            
            setting.url = url_txt
            
            try! realm.write() {
                realm.add(setting)
            }
            
            // 画面遷移
            let next = self.storyboard!.instantiateViewController(withIdentifier: "Top")
            self.present(next, animated: false, completion: nil)
            
        }

    }
    
}

