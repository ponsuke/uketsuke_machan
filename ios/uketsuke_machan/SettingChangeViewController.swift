//
//  SettingChangeViewController.swift
//  uketsuke_machan
//
//  Created by 深澤勝 on 2019/01/28.
//  Copyright © 2019年 northshore, inc. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class SettingChangeViewController: UIViewController {
    
    @IBOutlet weak var url: UITextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // 初期値を与える。
        let realm = try! Realm()
        let setting = realm.objects(SettingModel.self).first
        
        // 画面遷移
        if (setting != nil) {
            url.text = setting!.url
        }
        
    }
    
    @IBAction func save(_ sender: Any) {
        
        var url_txt: String = ""
        
        if "" != self.url.text! {
            url_txt = self.url.text!
        }
        
        // 入力チェック
        if url_txt != "" {
            
            let realm = try! Realm()
            
            try! realm.write {
                realm.deleteAll()
            }
            
            let setting = SettingModel()
            
            setting.url = url_txt
            
            try! realm.write() {
                realm.add(setting)
            }
            
            // 画面遷移
            let next = self.storyboard!.instantiateViewController(withIdentifier: "Top")
            self.present(next, animated: false, completion: nil)
            
        }
        
    }
    
}

