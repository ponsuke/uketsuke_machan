//
//  FinishPage.swift
//  mtg_receptionist
//
//  Created by 深澤勝 on 2018/11/09.
//  Copyright © 2018年 northshore, inc. All rights reserved.
//

import Foundation

import UIKit
import RealmSwift

class FinishPageViewController: UIViewController {
    
    var timer: Timer!
    var qr_url: String = ""

    @IBOutlet weak var welcome: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)
        
        // QRコード転送
        let realm = try! Realm()
        let setting = realm.objects(SettingModel.self).first
        self.qr_url = setting!.url + "/invitation/qr/check/"

        let url = URL(string: self.qr_url + QrCode)
        let request = URLRequest(url: url!)
        let session = URLSession.shared
        
        session.dataTask(with: request) { (data, response, error) in
            
            if error == nil, let data = data, let _ = response as? HTTPURLResponse {

                do {

                    let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: String]

                    if ("ok" == json["result"]) {

                        // TOP画面に移動
                        DispatchQueue.main.async {
                            self.timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(FinishPageViewController.changeView), userInfo: nil, repeats: true)
                        }

                    } else {
                        // エラー処理
                        // .. エラー画面に遷移する
                    }
                    
                } catch {
                    print("Serialize Error")
                }
                
            }
            
        }.resume()
        
    }
    
    @objc func changeView() {
        let next = storyboard!.instantiateViewController(withIdentifier: "Top")
        self.present(next, animated: true, completion: nil)
    }

}
